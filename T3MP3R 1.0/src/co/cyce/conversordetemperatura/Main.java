package co.cyce.conversordetemperatura;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Main extends Activity {
	
	private EditText eTFromValue;
	private EditText eTToValue;
	private Spinner spFromUnits;
	private Spinner spToUnits;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		eTFromValue = (EditText)findViewById(R.id.eTFromValue);
		eTToValue  = (EditText)findViewById(R.id.eTToValue);
		spFromUnits = (Spinner)findViewById(R.id.spFromUnits);
		spToUnits = (Spinner)findViewById(R.id.spToUnits);
		Button convertButton = (Button) findViewById(R.id.buConvert);
		
		convertButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String s = (eTFromValue.getText().toString());
				double fv = Double.valueOf(s.trim()).doubleValue();
				double tv = 0.0;
				String fu = String.valueOf(spFromUnits.getSelectedItem());
				String tu = String.valueOf(spToUnits.getSelectedItem());
				//eTToValue.setText(fu);	
				if(fu.equals("°C")){
					if(tu.equals("°F")){
						tv = 9.0/5.0*fv+32.0;
					}else if(tu.equals("K")){
						tv = fv+273.15;
					}else if(tu.equals("°R")){
						tv = (fv+273.15)*9.0/5.0;
					}else{
						tv = fv;
					}
				}else if(fu.equals("°F")){
					if(tu.equals("°C")){
						tv = (fv-32.0)*5.0/9.0;
					}else if(tu.equals("K")){
						tv = (fv-32.0)*5.0/9.0+273.15;
					}else if(tu.equals("°R")){
						tv = fv-32.0+(273.15)*9.0/5.0;
					}else{
						tv = fv;
					}
				}else if(fu.equals("K")){
						if(tu.equals("°C")){
							tv = fv-273.15;
						}else if(tu.equals("°F")){
							tv = 9.0/5.0*(fv-273.15)+32.0;
						}else if(tu.equals("°R")){
							tv = (fv)*9.0/5.0;
						}else{
							tv = fv;
						}			
				}else if(fu.equals("°R")){
					if(tu.equals("°C")){
						tv = 5.0/9.0*fv-273.15;
					}else if(tu.equals("°F")){
						tv = fv+32.0-(273.15)*9.0/5.0;
					}else if(tu.equals("K")){
						tv = (fv)*5.0/9.0;
					}else{
						tv = fv;
					}
				}
				eTToValue.setText(Double.toString(tv));				
			}
		});

		Button switchButton = (Button) findViewById(R.id.buSwitch);
		switchButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				int idf = spFromUnits.getSelectedItemPosition();
				int idt = spToUnits.getSelectedItemPosition();
				spToUnits.setSelection(idf);
				spFromUnits.setSelection(idt);
			}
		});
	}


	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
